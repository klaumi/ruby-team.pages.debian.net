---
author: Daniel Leidert
date: '2020-02-04T15:44:36+01:00'
layout: post
title: 'Ruby Team Sprint 2020 in Paris - Day One'
description: >-
  Day One of the Ruby Team Sprint in Paris is over. Topics covered have been
  discussing the Ruby 2.7 transition, dropping the Ruby interpreter dependency
  from libraries, optimization of the Salsa CI service, helping Kali Linux to
  reduce the differences between their and Debian's Ruby packages and much, much
  more ...
tags:
  - sprint
  - paris
  - ruby-paris
  - ruby2020
  - ruby-team
redirect_from:
  - /2020/02/04/ruby-sprint-paris-2020-day-1.html
---

The [Ruby Team Sprint 2020 in Paris][Sprint] started. A dozen of us joined,
some of them arriving just after attending [MiniDebCamp][MiniDebCamp] right
before [FOSDEM][FOSDEM] and FOSDEM itself.

<figure>
  <img style="width: 100%;" src="/assets/images/ruby-sprint-2020-day-1.jpeg" alt="Group photo of the Ruby Team Sprint 2020 in Paris">
  <figcaption>Group photo of the Ruby Team Sprint 2020 in Paris</figcaption>
</figure>

[Sprint]: https://wiki.debian.org/Teams/Ruby/Meeting/Paris2020
[MiniDebCamp]: https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp
[FOSDEM]: https://fosdem.org/2020/

Day One consisted of setting up at the venue, Campus 4 of the Sorbonne
University, as well as collecting and discussing our tasks for the next days, and
starting the work. The main goals so far for the sprint are:

  * Update packages in preparation for the Ruby 2.7 transition
  * Update packages for the [Rails 6 transition][wiki:Rails6]
  * Fix several testing migration issues
  * Improve the team's tools and workflow
    * Optimize usage of Salsa CI and reduce workload for Salsa service
    * Prevent breakages by minor transitions
  * Fix team specific issues
    * Remove the interpreter dependency from libraries
    * Handle rubygems integration warnings (working together with
      [Deivid Rodríguez][github:Deivid], upstream rubygems maintainer, who
      kindly agreed to join the sprint).
    * Optimize and improve installed gemspecs
  * Reduce the differences for Ruby packages on Kali Linux

[wiki:Rails6]: https://salsa.debian.org/ruby-team/rails/-/wikis/Transition-to-Rails-6-for-Debian-Bullseye
[github:Deivid]: https://github.com/deivid-rodriguez

There are more items on the list which will be discussed during the following
days.

At the end of Day One there is already a two digit number of both packages
uploaded and bugs approached and fixed, and we managed to go through half of
the topics that required discussion.

We hope to be able to keep up the good work and finish on Friday with a lot of
our goals to be reached, taking a big step ahead.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
