---
author: Daniel Leidert
date: '2019-12-07T23:15:56+01:00'
title: Update to packaging the Jekyll import tool
description: >-
  For moving a personal blog away from blogger a lot of work has been put into
  packaging the current versions of a lot of Jekyll plugins. To import all
  existing posts Jekyll provides a tool to read the blog backup and create
  post markup files.
tags:
  - packaging
  - jekyll
  - bundler
---

For moving my personal blog away from blogger I've put a lot of work into
packaging and/or updating (the most common) [Jekyll plugins] for Debian. To
ease the work further I began to [package] the [Jekyll importers]. But they
need some (yet) unpackaged gems. So I've created an issue to track the progress
and put my work on this package on hold. Yesterday @utkarsh2102-guest contacted
me and [asked me for more details]. So I've spent the last hours to track down
what actually needs to be done. And the news are a lot better than expected.

Only `fastercsv`, `reverse_markup`, `behance` and `unidecode` must be packaged
as (runtime dependencies).

## Using jekyll-import right now with bundler

If you are in need of these importers you can use this Gemfile:

```YAML
source "https://rubygems.org"
ruby RUBY_VERSION

gem "jekyll-import"
```
{: title="Gemfile"}

then set `GEM_HOME` to somewhere you are allowed to write to (e.g.
`$HOME/.gem/`) and run `bundle install` on the `Gemfile`:

```console
$ export GEM_HOME=$HOME/.gem/
$ bundle install
```

Then write your importer ([mine]) and run it with `ruby`.

## Using jekyll-import right now mixing Debian packages and bundler

If you want to minimize the installation of non-Debian provided gems you
probably want to run this on **Debian Sid**:

```console?prompt=$
$ apt-get install ruby-public-suffix ruby-concurrent ruby-eventmachine \
                  ruby-ffi ruby-rb-inotify ruby-listen ruby-nokogiri \
                  ruby-bundler ruby-concurrent ruby-http-parser.rb ruby-sass \
                  ruby-forwardable-extended jekyll
```
{: title="Install required packages for ruby-jekyll-import"}

This installs all the necessary ruby packages. Then run `bundle install` on the
following `Gemfile`:

```
source "https://rubygems.org"
ruby RUBY_VERSION

# This will help ensure the proper Jekyll version is running.
gem "jekyll", "~> 3.8.3.0"

# This will ensure we use the ruby packages in Sid.
gem "concurrent-ruby", "~> 1.0.5.0"
gem "eventmachine", "~> 1.0.7.0"
gem "ffi", "~> 1.9.10.0"
gem "listen", "~> 3.1.5.0"
gem "nokogiri", "~> 1.10.4.0"
gem "pathutil", "~> 0.16.1.0"
gem "public_suffix", "~> 3.0.3.0"
gem "rb-inotify", "~> 0.9.10.0"

# This will install jekyll-import.
gem "jekyll-import"
```
{: title="Gemfile for Debian users"}

[Jekyll plugins]: https://salsa.debian.org/explore/projects?tag=jekyll-plugin
[package]: https://salsa.debian.org/ruby-team/ruby-jekyll-import
[Jekyll importers]: https://import.jekyllrb.com/
[asked me for more details]: https://salsa.debian.org/ruby-team/ruby-jekyll-import/issues/1
[mine]: https://github.com/dleidert/www.wgdd.de/blob/master/tools/import_from_blogger.rb

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
