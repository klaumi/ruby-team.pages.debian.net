---
author: Daniel Leidert
date: '2020-02-07T14:47:21+01:00'
layout: post
title: Ruby Team Sprint 2020 in Paris - Day Four
description: >-
  Work progressed with the Ruby&nbs;p2.7 and Rails&nbsp;6 transitions. We also
  managed to almost finish the upload of Kali Ruby packages into the Debian
  repository.
tags:
- sprint
- paris
- ruby-paris
- ruby2020
- ruby-team
- ruby2.7
- gettogether
- faraday
- bundler
---

On day four the transition to Ruby&nbsp;2.7 and Rails&nbsp;6 went on. Minor
transitions took place too, for example the upload of `ruby-faraday 1.0` or the
upload of `bundler 2.1` featuring the (first) contributions by bundler's
upstream Deivid (yeah!). Further Red&nbsp;Hat's (and Debian's)
Marc&nbsp;Dequènes (Duck) joined us.

We are proud to report, that updating and/or uploading the Kali packages is
almost done. Most are in NEW or have already been accepted.

The Release team was contacted to start the Ruby&nbsp;2.7 transition and we
already have a [transition page][trans:ruby27]. However, the Python&nbsp;3.8
one is ongoing (almost finished) and the Release team does not want overlaps.
So hopefully we can upload `ruby-defaults` to Debian Unstable soon.

[trans:ruby27]: https://release.debian.org/transitions/html/ruby2.7.html

In the evening we got together for a well earned collective drink at Brewberry
Bar and dinner, joined by local Debian colleague Nicolas Dandrimont (olasd).

<figure>
  <img style="width: 100%;"
       src="/assets/images/ruby-sprint-2020-day-4.jpeg"
       alt="Group photo of the Ruby Team in Brewbarry Bar, Paris">
  <figcaption>Group photo of the Ruby Team in Brewberry Bar (Paris 2020)</figcaption>
</figure>

The evening ended at Paris' famous (but heavily damaged) Notre-Dame cathedral.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
