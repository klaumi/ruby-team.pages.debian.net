---
author: Daniel Leidert
date: '2020-02-07T11:19:39+01:00'
layout: post
title: Ruby Team Sprint 2020 in Paris - Day Three
description: >-
  Day three is over. Again a lot of packages have been updated and fixed.
  Still there was time to exchange key fingerprints for a key signing. Even some local
  university employees used the opportunity. Some more major topics have been solved
  as well.
tags:
- sprint
- paris
- ruby-paris
- ruby2020
- ruby-team
- ruby2.7
- gitlab
- salsa-ci
- keysigning
- piuparts
- leaf package
---
Day three of our sprint was dominated by hacking. In the morning an archive
wide rebuild against Ruby&nbsp;2.7 had finished. So the list of packages in need
of a fix for the upcoming transition got longer. Still we found/made some time
for a key exchange in the afternoon, in which even some local university
attendees participated. Further Georg gave a short talk how keysigning works
using `caff` and the current situation of keyservers, specifically
[`keys.openpgp.org`][keys.o.o] and [`hockeypuck`][hockeypuck]. (The traditional
SKS network plans to migrate to this software within this year.) 

[keys.o.o]: https://keys.openpgp.org/
[hockeypuck]: https://github.com/hockeypuck/hockeypuck

Regarding Salsa, Antonio was able to [fix][gem2deb:fix] `gem2deb` so our
extension packages finally build reprodicibly (Yeah!). The decision to disable
the `piuparts` job on Salsa was discussed again. The tool provides a major
functionality in question of preventing "toxic" uploads. But these issues
usually occur on quite rare occasions. We think the decision to enable the
`piuparts` job only for critical packages or on a case-by-case base is a
sensible approach. But we would of course prefer to not have to make this
decision just to go easy on Salsa's resources.

[gem2deb:fix]: https://salsa.debian.org/ruby-team/gem2deb/commit/8ea72d507f704b068640f67f37b3f1fc35ca4d98

Regarding the complex packaging situation of gitlab and the high likability to
break it by uploading new major releases we decided to upload new major
versions to `Experimental` only and enable a subset of `gitlab`'s tests to
discover breakages more easily.

Some leaf packages have been found during our Sprint days. This led to the
question how to identify candidates for an archive removal. It seems there is
no tool to check the whole archive for packages without any
reverse-dependencies (`Depends`, `Suggests`, `Recommends`, and `Build-Depends`).
The `reverse-depends` tool can do this for one package and would need to be run
against all team packages. Also we would like to identify packages, which have
low popcon values, few reverse dependencies, and could be replaced by more
recent packages, actively maintained by an upstream. We decided to pick up this
question again on our last days' discussion.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
