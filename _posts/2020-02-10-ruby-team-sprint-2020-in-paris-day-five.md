---
author: Daniel Leidert
date: '2020-02-10T20:00:00+01:00'
layout: post
title: Ruby Team Sprint 2020 in Paris - Day Five - We've brok^done it
description: >-
  The Ruby Team Sprint 2020 in Paris is over. Work progressed further, reaching
  our goal to upload the Kali Ruby packages and making a large step towards the
  Ruby&nbsp;2.7 transition. Some things got broken on the way but we also fixed
  them. We thank the sponsors: Debian and our DPL, Offensive Security, Sorbonne
  Université in Paris, and of course Cédric Boutillier.
tags:
- sprint
- paris
- ruby-paris
- ruby2020
- ruby-team
- ruby2.7
- gettogether
---

On our last day we met like every day before, working on our packages, fixing
and uploading them. The transitions went on. Antonio, Utkarsh, Lucas, Deivid,
and Cédric took some time to examine the `gem2deb` bug reports. We uploaded the
last missing Kali Ruby package. And we had our last discussion, covering the
future of the team and an evaluation of the [sprint][sprint]:

[sprint]: https://wiki.debian.org/Teams/Ruby/Meeting/Paris2020

<figure>
  <img style="width: 100%;"
       src="/assets/images/ruby-sprint-2020-day-5_1920.jpeg"
       alt="Last discussion of the Ruby Team Sprint 2020 in Paris">
  <figcaption>Last discussion round of the Ruby Team Sprint 2020 in Paris</figcaption>
</figure>

As a result:

* We will examine ways to find leaf packages.
* We plan to organize another sprint next year right before the release
  freeze, probably again about *FOSDEM* time. We tend to have it in Berlin
  but will explore the locations available and the costs.
* We will have monthly IRC meetings.

We think the sprint was a success. Some stuff got (intentionally and less
intentionally) broken on the way. And also a lot of stuff got fixed.
Eventually we made our step towards a successful Ruby&nbsp;2.7 transition.

So we want to **thank**

* the [Debian project][debian] and our DPL Sam for sponsoring the event,
* [Offensive Security][offsec] for sponsoring the event too,
* Sorbonne Université and [LPSM][lpsm] for hosting us,
* Cédric Boutillier for organizing the sprint and kindly hosting us,
* and really everyone who attended, making this a success: Antonio, Abhijith,
  Georg, Utkarsh, Balu, Praveen, Sruthi, Marc, Lucas, Cédric, Sebastien,
  Deivid, Daniel.

[debian]: https://www.debian.org/
[offsec]: https://www.offensive-security.com/
[lpsm]: https://www.lpsm.paris/

<figure>
  <img style="width: 100%;"
       src="/assets/images/ruby-sprint-2020-group.jpeg"
       alt="Group photo of the attendees of the Ruby Team Sprint 2020 in Paris">
  <figcaption>Group photo; from the left in the Back: Antonio, Abhijith, Georg, Utkarsh,
  Balu, Praveen, Sruthi, Josy. And in the Front: Marc, Lucas, Cédric, Sebastien,
  Deivid, Daniel.</figcaption>
</figure>

In the evening we finally closed the venue which hosted us for 5 days, cleaned
up, and went for a last beer together (at least for now). Some of us will stay
in Paris a few days longer and finally get to see the city.

<figure>
  <img style="width: 100%;"
       src="/assets/images/paris-eiffeltower_1280.jpg"
       alt="Eiffel tower in Paris">
  <figcaption>Eiffel Tower Paris (February 2020)</figcaption>
</figure>

**Goodbye Paris and save travels to everyone. It was a pleasure.**

*[IRC]: Internet Relay Chat
*[DPL]: Debian Project Leader
*[LPSM]: Laboratoire de Probabilités, Statistique et Modélisation

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
