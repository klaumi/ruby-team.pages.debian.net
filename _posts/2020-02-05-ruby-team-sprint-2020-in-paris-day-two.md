---
author: Daniel Leidert
date: '2020-02-05T10:31:09+01:00'
layout: post
title: Ruby Team Sprint 2020 in Paris - Day Two
description: Day Two of Debian's Ruby Team Sprint 2020 is over, a day of focus and
  uploads. Besides fixing almost all arch:any packages for the Ruby 2.7 transition,
  we were able to find a solution to reduce the Salsa CI pipeline resources for our
  group.
tags:
  - sprint
  - paris
  - ruby-paris
  - ruby2020
  - ruby-team
  - ruby2.7
  - rails6
  - salsa-ci
---
Day Two of the Ruby Team Sprint 2020 in Paris is over. Again we were able to
tackle our main goals.  There was a lot of silence as everybody was focused on
their tasks. At the end we took an hour to discuss our open topics.

As a result of Day Two we achieved the following:

## Ruby&nbsp;2.7 transition

Most key `arch:any` packages are now fixed or being worked on. The next step
will be to rebuild all `arch:all` packages to find build failures.

## Rails&nbsp;6 transition

The latest version has been [uploaded to Debian Experimental][deb:rails6]. The
[wiki][wiki:rails6] informs about the packages required to be updated and/or
fixed.

[deb:rails6]: https://packages.qa.debian.org/r/rails/news/20200204T220012Z.html
[wiki:rails6]: https://salsa.debian.org/ruby-team/rails/-/wikis/Transition-to-Rails-6-for-Debian-Bullseye

## Improving/optimizing our usage of the Salsa CI pipeline

We tend to disable the `blhc`, `reprotest`, and `piuparts` jobs of the Salsa CI
pipeline by default for all group projects. For C-extension packages `bhlc` and
`reprotest` will be re-enabled by default. `piuparts` could be enabled for some
critical packages.

We first thought about setting the relevant pipeline variables
(`SALSA_CI_DISABLE_*`) to zero via group variables and let maintainers decide
on a case-by-case base to re-enable them via project variables. But it seems
more convenient and apparent to instead create our own pipeline file (include
the Salsa CI pipeline and set variables there) and include it from all our
projects.

Maintainers can still temporarily enable or disable jobs by running a pipeline
using the web-interface or API passing the variables along.

## New items on our TODO list for next days

* Start bug triage and fixing for gem2deb.
* Productize team tools.
* Fix reprotest failing for all our C-extensions (culprit found).
* Finish the Kali Ruby packages.
* Complete source-only uploads for all accepted packages from NEW.

## Package uploads and bugs fixed

The number is hard to track as members of the group worked all day and even
late in the evening. It is safe to say that it is again a two digit number.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
