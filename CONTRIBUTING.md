---
layout: page
title: Contribution Guidelines
Description: >
  ... how to contribute to the work of the Debian Ruby Team
permalink: /contribute/
---

# How to help the Ruby-Team

For a Debian enthusiast with basic Ruby knowledge there are many ways to contribute to Debian Ruby team, which includes (and is not limited to):

##Identifying and reporting bugs

If you are a user of the packages maintained by Debian Ruby team, you can help by reporting the bugs you find in the packages using the [Debian Bug Tracker](http://bugs.debian.org/){:target="_blank"} or [reportbug](https://wiki.debian.org/reportbug) tool. You can also add *wishlist bugs* about changes/features you would like to be added to the package so as to make it better to use.


## Bug triaging

You can also help to triage bugs, by choosing bugs from  
[https://bugs.debian.org/pkg-ruby-extras-maintainers@lists.alioth.debian.org](https://bugs.debian.org/pkg-ruby-extras-maintainers@lists.alioth.debian.org)  
or  
[https://udd.debian.org/dmd/?email1=pkg-ruby-extras-maintainers%40lists.alioth.debian.org&email2=&email3=&packages=&ignpackages=&format=html#todo](https://udd.debian.org/dmd/?email1=pkg-ruby-extras-maintainers%40lists.alioth.debian.org&email2=&email3=&packages=&ignpackages=&format=html#todo),  

and try to

1. reproduce them on your local machine / clean chroot as needed
2. confirm bug's existence and validity
3. Tag bugs appropriately
4. Close invalid ones


##Help Upgrading packages maintained by the team or building new ones

Helping the ruby-team upgrading packages maintained by the team or building new ones is always welcome. Our [Starters Guide](./_contributing/Starters_Guide_01.html) will give you an overview of our workflow.

If you are already an expert in Debian packaging and in using *Git* and *salsa.debian.org*, you may find the [ruby-team pages in the Debian-Wiki](https://wiki.debian.org/Teams/Ruby/Packaging){:target="_blank"} more straight forward. 













<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
