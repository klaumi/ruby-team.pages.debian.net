---
title: Workflow Changes when becomming a Team-Member
Description: >
  ... what changes when you finally decide to become member of the ruby team
layout: page
subcollection: Starters_Guide
---

# When you finally decide to become member of the ruby team

## How to become member of the team

_(yet to be written)_

## Duties of team members

_(yet to be written)_

## What changes regarding the workflow with salsa?

Mainly one thing: You do not need to fork a project at the start. You can clone the team' repository of a package out of the teams namespace and push back your changes directly into the team's repository of a package.

The GitLab ( the software Salsa uses) knows different roles. The team will probably let you start with the basic rights. Ask the team what you are allowed to do and how you should adjust your workflow!

Especially when preparing a big version upgrade for a package (like *1.2.3* to *2.0.1*) it might be a good idea to create a new branch and start there.


