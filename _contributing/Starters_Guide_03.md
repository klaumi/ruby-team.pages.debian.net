---
title: Preparations Part II:
Description: >
  ... Setting up your building and testing environment
layout: page
subcollection: Starters_Guide
---

# Preparations Part II: Setting up the building and testing environment 

If you built debian packages for personal use before you probably  used something like `dpkg-buildpackage` or `debuild` on your local machine to build the source and binary packages. 

Your daily used machine normally runs a stable Debian system, maybe testing or a mixture of both with some unstable packages. To build and test an official Debian package this needs to happen in an clean unstable environment. So you will have to build and test using chroots or lxc-containers.

##1. ruby-meta

The ruby-team offers a set of scripts called *meta* to make setting up such an environment for building and testing a little bit easier for you. It is located in its own Salsa-repository: [salsa.debian.org/ruby-team/meta](https://salsa.debian.org/ruby-team/meta){:target="_blank"}.

Clone this repository to the local computer where you want to build and test packages:

	git clone git@salsa.debian.org:ruby-team/meta.git meta

Enter the newly created directory *meta*, read the *README.md* and then create your building and testing environment by running the *setup*-script (which utilizes sbuild, autopkgtest and lxc to do so):

	./setup

The script will install missing packages and therefore ask for your user password. It case the script fails, you have to install some missing packages by yourself and rerun the script. At the end the script informs you:

	Finished testbed setup for unstable: <Date>

From now on you can use the *build*-script in *meta* repository from within your package's local git repository to build and test in a clean environment.

##2. lintian options

For testing the packages lintian should be called with some extra options to make its report more verbose and pedantic. For using the *build* script of ruby-team's meta-repository you should make this options standard for sbuild by adding them to your `~/.sbuildrc`:

	# this enables all lintian tags:
	$lintian_opts = ['-i', '-I', '-E', '--pedantic'];

***

And after all this preparations: You are finally ready to start your first project!

