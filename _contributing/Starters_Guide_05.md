---
title: Case II: Preparing a new Package
Description: >
  ... How to help creating a new package for the ruby-team
layout: page
subcollection: Starters_Guide
---

#Case II: Preparing a new Package

The workflow in this case is very similar to the one described above for upgrading a package but the beginning is a little bit different: There is no remote repository to clone and start with.

So you have to create a new git repository for your package on your local machine first.

##1. Debian package from a *rubygems* Package

In case you want to build a Debian package for a piece of software that is available at rubygems you can let `gem2deb` do most of the work. Go to the directory where you want your new git repository to be created and run:

	gem2deb -p <debian-package-name> -g --purge <gem-Package>

This will run `gem fetch <gem-Package>` to download the package from rubygems, build a git-tree for the package, build the debian-source- and binary-packages, import the prestine-tar to git-tree and then delete all crap outside the git-tree. As result you have a git-tree with 3 branches:

* `master` 
* `pristine-tar` 
* `upstream` 

and 2 tags:

* `debian/<upstream-version>-1` 
* `upstream/<upstream-version>`

Because on Salsa the debian tag is only set when a package is released you should remove this tag for now. Look up the name of the tag:

	git tag

and remove it:

	git tag -d debian/upstream-version>-1


##2. Debian Package from an upstream Archive-File
	
In case the software you want to package is only available upstream as some gzip, bzip2, lzma or xz compressed tar archive or as zip archive you have to do a little more. 

First check if the package `python3-requests` is installed on your system:

	dpkg -l python3-requests

If it is not install it:

	sudo apt install python3-requests

Then go to the directory where you want to store your local git-repository of the package and create a directory with the name of the debian package you want to build and change into it:

	mkdir <package-name> && $_

Now place this directory under Git's version control, i.e. make an empty Git repository:

	git init

Next point your Browser to the site where the upstream archive can be downloaded and copy the link for the archive you want to use.

Then go back to your terminal and use *gbp* to download and import this archive to your empty git repository:

	gbp import-orig --interactive --upstream-version=<upstream.program.version> --pristine-tar <https://place-your-copied-archive-url.here>

This lets *gbp* download the upstream archive, import it into your git directory, create and populate 3 branches *master*, *upstream* and *pristine-tar* and tag the upstream version.

>__Note:__ If the url you gave *gbp* to import is an url-redirect the above *gbp*-command will not work. In such a case you have to download the archive yourself, place it into the parent directory of you repository an run from within your repository:
> 
>	gbp import-orig --interactive --upstream-version=<upstream.program.version> --pristine-tar ../<downloaded-archive.tar.xzy>

After the *gbp*-import you will notice that the *debian* directory and all the files in it are still missing in your repository.

So let's create it:

	mkdir debian

and let `dh_make` populate it with templates:

	dh_make -p <package-name>_<version> --addmissing

`dh_make` asks you some questions about the package, chooses the templates to copy and writes a first entry into the copyright file. 

>__Note__: To avoid questions you can give `dh_make` more options. Namely `-c <copyright-typ>` might be interesting if you know the license typ of the program because `dh_make` does not ask for it.

After `dh_make` has populated the debian directory you have to make git aware of all the new files:

	git add debian/.

and commit all changes:

	git commit -a -m "create and and let dh_make fill the debian directory"

##3. Creating the remote Salsa repository

Wether you have created your local git repository from a rubygem package or from an upstream archive before making any further changes in the debian directory you should setup your remote Salsa repository.

Point your browser to `https://salsa.debian.org/<your-username>`, log in and create a new project with the name of your new debian package via Salsa's web-interface.

Then go back to your terminal and tell git that the created new and empty git repository at Salsa is your remote origin:

	git remote add origin git@salsa.debian.org:<your-username>/<project-name>.git

and push your local repository to the remote:

	gbp push

or:

	git push -u --all --follow-tags

Now you can start ...

##4. working on the debian directory and all further tasks

Especially when you created your repository from an upstream archive, adjusting the files in the debian directory will be much more work than upgrading an existing package. You have to touch every single file, work on it or decide to delete it. You have to set dependencies and build-dependencies for the package, the description and so on. Check the upstream docs to get some of the information you need. 

Sadly `dh_make` has no template files for ruby packages. You may want to have a look at other ruby repositories' debian directories to get some ideas.

Nevertheless: Concerning the workflow with git and gbp is similar to upgrading a package: You have to commit your changes with comment:

	git commit -a -m "short description of your changes"

You have to inform git of newly created files:

	git add debian/<newfile>

And at the end of your work you have to push your changes to your remote repository:

	gbp push

The same applies for building and testing the package: It is similar to upgrading an existing package.

Only getting your work finally into a ruby team's repository will differ slightly: You cannot make a merge request, the team has to fork your repository into their namespace.


