---
title: Case 1: Upgrading a Package
Description: >
  ... how to help upgrading an existing Debian package
layout: page
subcollection: Starters_Guide
---

# Case 1: Help upgrading an existing package

First of all: When you want to help upgrading an existing package make the debian-ruby team know about it by posting your intention at the debian-ruby mailing-list! It also helps to file a bugreport for the package about not being upgraded.

##1. Fork existing GIT repository of the package

The git repositories of the packages maintained by the ruby-team are stored in `https://salsa.debian.org/ruby-team/<package-name>`. 

As the ruby-team doesn't know you and you don't know them the easiest way to start your upgrade attempt will be to fork the ruby-teams repository of the package to your own namespace at Salsa. To do so, log in at salsa.debian.org, go to the ruby-teams package repository `https://salsa.debian.org/ruby-team/<package-name>` and click on the small *Fork*-Button on the upper right side of the page (beside the package name). Choose your own namespace as the one to fork to.

You can now work with this fork in your own namespace at `https://salsa.debian.org/<yourname>/<package-name>` without any fear of destroying something. After you are done you can make a merge request for the teams repository to get your changes back there.

##2. Next clone (download) your Salsa Git repository to your local computer. 

A debian package repository normally has a lot of tags and at least 3 branches: 

* _master_ : that's where you want to work
* _pristine-tar_ : where the source-packages are stored
* _upstream_ : where the upstream source tree is stored

The easiest way to get all these tags and branches cloned to your computer is to use *gbp* instead of *git*. Go to the location where you want to store the directory with your local git-tree of the package and run:

	gbp clone --pristine-tar git@salsa.debian.org:<yourname>/<package-name>.git <package-name>

Then change to the newly created directory

	cd <package-name>

and start your changes.

##3. Import the new upstream version

This should be easy if the information in the debian/watch file still works. You can check if the new upstream version is found with:

	uscan --no-download --verbose

If all works run:

	gbp import-orig --pristine-tar --uscan

This will download the upstream source package and create a Debian source-package in the parent directory with uscan and then extract this source-package into the current directory and import it into the master branch. You can also run these two steps separately:

	uscan --verbose
	gbp import-orig --pristine-tar ../<package>_<new.version>.orig.tar.gz

Now update the debian changelog file to the new version:

	gbp dch -a

and open the file `debian/changelog` with an text-editor of your choice. In the first line replace the word `testing` with `UNRELEASED` and as first entry of changes add `* Team Upload`. Then make git aware of this change by running:

	git commit -a -m "set package as unreleased and team-upload"

Last, to update your remote Salsa repository you want to push all changes in all branches using gbp:

	gbp push

or using git:

	git push -u --all --follow-tags


##4. Make all necessary changes in the debian-directory

__Remember:__ All changes are only done in the debian-directory! Do not touch the upstream code! If you have to modify it, make patches using quilt. See:  

* [Debian Wiki: Using Quilt](https://wiki.debian.org/UsingQuilt){:target="_blank"},  
* [New Maintainers' Guide: Modifying the source](https://www.debian.org/doc/manuals/maint-guide/modify.html){:target="_blank"}
  
  and last not least: 
 
* [Working with Patches](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.patches.html) to see how patches are managed with Git and `gbp pq`.

Normally all changes should be done in the master branch of the repository. To check which branch the directory-tree in front of you represents run:

	git branch

the master should be highlighted. If it's not change to master with

	git checkout master

Git will then rebuild the tree to match the master branch.

When upgrading the package you have to check all files in the debian-directory, adjust at least the dates in the copyright-file and eventually existing patches. (In the later case check if there already exists a patch-queue branch in the repository of your package!) Also consult the upstream changelog to find out what might have changed in the upstream code that affects the packaging.

Whenever you have done all changes concerning a specific topic you should document your changes in the debian/changelog file and make git aware of your changes by running:

	git commit -a -m "short description of your changes"

from within the main-directory of your git repository.

>__Note:__ You can also run the `git`-command without the `-m` option. In this case an editor (*vi* if not configured otherwise) will pop up to type in your description of the changes. If you give  no description, the changes will not be recorded by git.  
*(To remind you of the essential `vi` commands: `<I>` or `<INS>` will put the program into input-mode, `<ESC>` will return to command-mode, `:wq` while in command-mode will save the changes and exit the program.)*  
You can change the editor used by Git by setting the `GIT_EDITOR` environment variable for your shell, or by configuring Git:  
*`git config --global core.editor <my-editor>`*

Whenever you create a new file, you have to make git aware of this file:

	git add debian/<newfile>

When you think you are done with all your changes or when you want to have a longer break you can push your local changes in the actual (master-)branch back to your remote Salsa repository with:

	gbp push

To see what will happen, you may try first:

	gbp push --dry-run

When you are working at more than one local machines or when you are working together with other people on the same remote repository every time you start working you have to make sure that the local git tree matches the remote one by running:

	gbp pull --redo-pq

and you should push your changes whenever you want to change the local machine or pause working.



##5. Building and testing the package:

The moment of truth: When you finally think you are done with all necessary changes in the debian directory you try to build the package and test it. If you followed the advices in *Preparations Part II*, you only need to run the *build*-script included in the ruby-teams *meta* repository from within the local git repository of the package you want to have built;

	~/<path-where-you-cloned-meta>/build

The script will run the necessary commands to build (sbuild) and test (autopkgtest, lintian) the package and also will rebuild and test all packages (build-)dependening on the package you just built. The final part is essential when trying to build and upload a major upgrade of a package.

This build process will leave your local git repository unchanged, its products can be found in the directory `build-area` in the parent folder of your repository.

And of course something will go wrong! Maybe the package-building itself doesn't work at all, some tests may fail or at least lintian will grumble about something. In all these cases you need to find the reason for the problem, go back to work on the files in the debian directory and try to fix the problem. If you are not sure how to do so: Never say die! Ask the debian-ruby mailing-list!

When you think you fixed all problems run the *build*-script again.

##6. Merging to ruby-teams repository, fetching and merging back changes

When building and testing the package finally works, don't forget to push your latest changes to your remote repository:

	gbp push

Then you should brief the ruby-team that you have finished upgrading the package and create a *merge request* for your (remote) repository into the ruby-teams repository via Salsa's web-interface: Log in, go to the teams repository of the package and choose *Merge Requests* on the left dashbar. 

You have to create one merge requests for every branch you worked on since you forked the teams repository, which were: _upstream_, _pristine-tar_ and _master_.

The team members will inspect the changes you made and may accept them, may change things by themselves or may want you to change things. 

In any case before you start working on your repository again you have to make sure that it still matches the teams repository. Therefore you need to add the teams repository to yours:

	git remote add team git@salsa.debian.org:ruby-team/<package-name>.git

so that is available for git under the name *team*. Check with:

	git remote -v

You can now update your local repository and get the teams content by running:

	git fetch --all

>__Note:__ This will not pull any changes to your repository, fetch only downloads objects and refs from another repository.

You can now check the changes between your master branch and the one in the team's repository:

	git diff master team/master

When there are changes in the team's repository you should merge the team's master branch into your master branch with:

	git merge team/master

If something seems to go wrong you can always abort with:

	git merge --abort

When the merging went well, you can push the changes to your remote repository:

	gbp push

and start working on your repository again.

