---
title: Ruby Packaging for Starters
Description: >
  ... Overview of the general workflow of the Debian Ruby Team's Packaging Practice for Beginners 
layout: page
subcollection: Starters_Guide
---

# Ruby Packaging for Starters

This document will try to give a short description of the general workflow of upgrading and creating Debian packages for the ruby-team  for beginners. 

If you are already an expert in Debian packaging and in using *Git* and *salsa.debian.org*, you may find the [ruby-team pages in the Debian-Wiki](https://wiki.debian.org/Teams/Ruby/Packaging){:target="_blank"} more straight forward. 

For this workflow description we only assume that you are a little bit familiar with the Debian package management and may have built one or another Debian package for personal use before. You should have had installed at least the packages

* `build-essential`
* `maint-guide` and
* `developers-reference`

and you should have taken a closer look into the installed Documents 

* [New Maintainers' Guide](file:/usr/share/doc/maint-guide/html/index.en.html){:target="_blank"} and 
* [Developer's Reference](file:/usr/share/developers-reference/index.html){:target="_blank"}

as well as into the [Ruby pages of the Debian Wiki](https://wiki.debian.org/Teams/Ruby/){:target="_blank"}

It will also be useful to set your name and the email-address you want to use developing packages in your `~/bashrc` (if you have not already done so):

	DEBFULLNAME="your name"
	DEBEMAIL=youremail@provider.com

**Communication** in the ruby-team mainly happens via *mailing list*:   

[debian-ruby@lists.debian.org](https://lists.debian.org/debian-ruby/){:target="_blank"}. 

So it is a good start to subscribe this list.

There is also an *IRC channel* where team members hang around and where you may find help:  

`#debian-ruby on irc.debian.org (OFTC)`  
&nbsp;

All done? Then let's get started!  
&nbsp;

## Table of Contents:

* [Preparations Part I: Get ready to work with Git and Salsa](./Starters_Guide_02.html)
* [Preparations Part II: Setting up the building and testing Environment](./Starters_Guide_03.html)
* [Case I: Help upgrading an *existing* Debian Package](./Starters_Guide_04.html)
* [Case II: Preparing a *new* Debian Package](./Starters_Guide_05.html)
* [Troubleshooting](./Starters_Guide_06.html)
* [When you finally decide to become member of the ruby team ...](./Starters_Guide_07.html)


