---
title: Troubleshooting
Description: >
  ... some answers in case something went wrong 
layout: page
subcollection: Starters_Guide
---

##Troubleshooting

####I cloned my local repository from the team's repository instead of forking it into my namespace at Salsa. Now I can not push.

The easiest way to fix this would be to ask the team for membership. 

If you don't want to do so, you have to fork the team's repository of you package to your namespace now and then tell git, that the repository in your namespace is the origin for your local repository.

First check the remote names:

	$ git remote -v
	origin	git@salsa.debian.org:ruby-team/<package-name>.git (fetch)
	origin	git@salsa.debian.org:ruby-team/<package-name>.git (push)

then set the new origin:

	git remote set-url origin git@salsa.debian.org:<your_username>/<package-name>.git

and add the team's repository again under the name *team*, so that you can fetch changes froom there:

	git remote add team git@salsa.debian.org:ruby-team/<package-name>.git

Now check again:

	$ git remote -v
	origin	git@salsa.debian.org:<your_username>/<package-name>.git (fetch)
	origin	git@salsa.debian.org:<your_username>/<package-name>.git (push)
	team	git@salsa.debian.org:ruby-team/<package-name>.git (fetch)
	team	git@salsa.debian.org:ruby-team/<package-name>.git (push)


####Instead of forking I imported the team's repository into my namespace at Salsa. Now I can not make a merge request.

There are two possibilities to fix this:

Either you rename your remote repository, fork the team's repository into your namespace (which will now have the same name as the old one and therefore be identified by git as the origin) and push you changes again with gbp or git.

Or use `libgitlab-api-v4-perl` to set the fork-property of your remote repository:

	gitlab-api-v4 set_project_fork <your_username>/<package-name> ruby-team/<package-name>

You can now place a merge request through Salsa's web-interface.
