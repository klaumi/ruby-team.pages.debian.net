---
title: Preparations Part I: 
Description: >
  ... Get ready to work with Git and Salsa
layout: page
subcollection: Starters_Guide
---

# Preparations Part I: Get ready to work with Git and Salsa

The packages maintained by the ruby team are developed using Git repositories at salsa.debian.org. 

##1.

At first you have to have installed Git itself and a suite to help with Debian packages in Git repositories:

	sudo apt install git git-buildpackage pristine-tar cowbuilder python3-requests

> __Note:__ In case you have never worked with git before the small and free ebook [Learning GIT](https://riptutorial.com/ebook/git){:target="_blank"} from [riptutorial.com](https://riptutorial.com){:target="_blank"} may give you an impression how this version-management-system generally works. But of course there are other tutorials as well.

##2.

Next you have to get yourself an account at [salsa.debian.org](https://salsa.debian.org){:target="_blank"}. With this account you get personal webspace for your projects which you can access in Salsa's web-interface at: `https://salsa.debian.org/<your-user-name>`. 

> __Note:__ When you are logged in at *salsa.debian.org* you will find a dashboard menu on the left side of the page to interact with Salsa's GitLab.

Normally you don't want to work with the web-interface of Salsa but clone a repository from there to your local machine, work here and push your changes back to Salsa after you are done. 

Especially when you are pushing code, Salsa needs to be sure that the machine you are working at is really yours (even if you may think it might be the other way round after days of work). Therefore you need to create a ssh-key (or use an existing one from `~/ssh/id_rsa.pub`) and add the public-key to your Salsa account. Run:

	ssh-keygen

to create an ssh-key, and upload the public key, which ends with `.pub` via Salsa's web-interface: Go to *User settings* and choose *ssh-keys* in the left dash-bar. 

If you picked a custom username for Salsa which differs from your username on your local machine you can add the following to `~/.ssh/config` (if the file does not exist yet just create it):

	host salsa.debian.org
	User <your-salsa-username>
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/<your_custom_name_git_rsa>


##3.

To provide the possibility of direct interaction with Salsa's GitLab API (which may be usefull at some point) you need to install the package libgitlab-api-v4-perl:

	sudo apt install libgitlab-api-v4-perl

To make the command-line-client work with Salsa you have to create a personal access token on salsa via web-interface: Go to *User settings*, choose *Access tokens* in the left dash-bar, then choose *with scope: api* and copy the created token. Run:

	gitlab-api-v4 configure

and add the token. 

>__Note:__ You will NOT see your input! Use copy and paste! The configured token is stored in the file `~/.gitlab-api-v4-config`.



